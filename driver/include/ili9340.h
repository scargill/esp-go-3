/*
 * File       : ili9340.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _ILI9340_H_
#define _ILI9340_H_

void IFA iliStartup();
void IFA ili9340_drawPixel(int16_t x, int16_t y, uint16_t color);
void IFA ili9340_drawLine(int16_t x0, int16_t y0,int16_t x1, int16_t y1,uint16_t color);
void IFA ili9340_drawRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
void IFA ili9340_fillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color,uint16_t bcolor);
void IFA ili9340_setBackColor(uint16_t col);
void IFA ili9340_setFrontColor(uint16_t col);
void IFA ili9340_drawString(uint16_t x, uint16_t y, const char *text);
void IFA ili9340GCLDChar(uint8_t ch);
void IFA ili9340_setRotation(uint8_t m);
void IFA ili9340GCLDString(uint8_t* st);

void IFA vt100_puts(const char *str);
#endif /* _ILI9340_H_ */
