/*
 * File       : sht.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _SHT_H_
#define _SHT_H_

void IFA wireRead_sht30(int *tp, int *hum, int *htp);

#endif /* _SHT_H_ */
