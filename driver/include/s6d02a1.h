/*
 * File       : s6d02a1.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _S6D02A1_H_
#define _S6D02A1_H_

void IFA s6d02a1Dot(uint16_t x, uint16_t y);
void IFA s6d02a1FillRect(int16_t x, int16_t y, int16_t w, int16_t h);
void IFA s6d02a1Startup(const uint8_t *ssdp);
void IFA s6d02a1SetRotation(uint8_t m);
void IFA s6da1scope(uint8_t line, uint8_t val);
int IFA s6d02a1SetScrollWindow(uint8_t TopFixHeight, uint8_t BotFixHeight);
void IFA s6d02a1VScroll(uint8_t vsp);
void IFA s6d02a1ExitScrollMode();
void IFA s6d02a1Rect(int16_t x, int16_t y, int16_t w, int16_t h);
void IFA s6d02a1DrawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1);
void IFA s6d02a1Fore(uint16_t r, uint16_t g, uint16_t b);
void IFA s6d02a1Back(uint16_t r, uint16_t g, uint16_t b);
void IFA s6d02a1Cursor(uint8_t x, uint8_t y);
void IFA s6d02a1GCLDFont(uint8_t font);

#endif /* _S6D02A1_H_ */
