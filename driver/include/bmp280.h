/*
 * File       : bmp280.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _BMP280_H_
#define _BMP280_H_
#include <c_types.h>

int ICACHE_FLASH_ATTR BMP280_initialize(char addr);
int16_t ICACHE_FLASH_ATTR BMP280_getTemperature();
int16_t ICACHE_FLASH_ATTR  BMP280_getPressure();

#endif /* _BMP280_H_ */
