/*
 * s6d02a1.c
 *
 *  Basic s6d02a1 access adapted from original segment here
 *  https://www.mgo-tec.com/blog-entry-adafruit-oled-s6d02a1-esp-wroom-nonlib.html
 */

// In this SPI Driver:
// WEMOS D6 ESP8266 GPIO12 is MISO (din) - not used here
// WEMOS D7 ESP8266 GPIO13 is MOSI (dout)
// WEMOS D5 ESP8266 GPIO14 is clock
// WEMOS D8 ESP8266 GPIO15 is CS/SS = not used here - assume that CS tied low, reset is tied to Processor reset
// WEMOS D0 ESP8266 GPIO16 as the D/C line.
// 5v power and ground and also 3v3 to the backlight - NOT 5v to backlight

#include "aidan_and_petes.h"
#include "easygpio/easygpio.h"
#include <math.h>
#include "spi.h"
#include "glcd_fonts.h"
#include "ssd1351.h"
#include "s6d02a1.h"

#define QDTech_TFTWIDTH  128
#define QDTech_TFTHEIGHT 160

// Some used command definitions kept from original
#define QDTech_INVOFF  0x20
#define QDTech_INVON   0x21
#define QDTech_DISPOFF 0x28
#define QDTech_DISPON  0x29
#define QDTech_CASET   0x2A
#define QDTech_RASET   0x2B
#define QDTech_RAMWR   0x2C
#define QDTech_RAMRD   0x2E

#define QDTech_PTLAR   0x30
#define QDTech_COLMOD  0x3A
#define QDTech_MADCTL  0x36

// Basic colour definitions
#define	QDTech_BLACK   0x0000
#define	QDTech_RED     0xF800
#define	QDTech_GREEN   0x07E0
#define	QDTech_BLUE    0x001F
#define QDTech_YELLOW  0xFFE0
#define QDTech_MAGENTA 0xF81F
#define QDTech_CYAN    0x07FF
#define QDTech_WHITE   0xFFFF
#define QDTech_GREY    0x632C

#define QDTech_NORON 0X13 		//NORMAL DISPLAY MODE
#define QDTech_VSCRSADD 0x37	//Vertical Scrolling Start Address
#define QDTech_VSCRDEF	0x33	// Set v scroll window

// a little experiment
uint8_t qd_scopepos=159; // ready to wrap
uint8_t qd_scopelines[10];

#define MADCTL_MY  0x80
#define MADCTL_MX  0x40
#define MADCTL_MV  0x20
#define MADCTL_ML  0x10
#define MADCTL_RGB 0x00
#define MADCTL_BGR 0x08
#define MADCTL_MH  0x04


// experiment from adafruit - header info for st7735
// for 1.44 and mini
#define ST7735_TFTWIDTH_128  128
// for mini
//#define ST7735_TFTWIDTH_80   80
// for 1.44" display
//#define ST7735_TFTHEIGHT_128 128
// for 1.8" and mini display
#define ST7735_TFTHEIGHT_160  160

#define ST7735_NOP     0x00
#define ST7735_SWRESET 0x01
#define ST7735_RDDID   0x04
#define ST7735_RDDST   0x09

#define ST7735_SLPIN   0x10
#define ST7735_SLPOUT  0x11
#define ST7735_PTLON   0x12
#define ST7735_NORON   0x13

#define ST7735_INVOFF  0x20
#define ST7735_INVON   0x21
#define ST7735_DISPOFF 0x28
#define ST7735_DISPON  0x29
#define ST7735_CASET   0x2A
#define ST7735_RASET   0x2B
#define ST7735_RAMWR   0x2C
#define ST7735_RAMRD   0x2E

#define ST7735_PTLAR   0x30
#define ST7735_COLMOD  0x3A
#define ST7735_MADCTL  0x36

#define ST7735_FRMCTR1 0xB1
#define ST7735_FRMCTR2 0xB2
#define ST7735_FRMCTR3 0xB3
#define ST7735_INVCTR  0xB4
#define ST7735_DISSET5 0xB6

#define ST7735_PWCTR1  0xC0
#define ST7735_PWCTR2  0xC1
#define ST7735_PWCTR3  0xC2
#define ST7735_PWCTR4  0xC3
#define ST7735_PWCTR5  0xC4
#define ST7735_VMCTR1  0xC5

#define ST7735_RDID1   0xDA
#define ST7735_RDID2   0xDB
#define ST7735_RDID3   0xDC
#define ST7735_RDID4   0xDD

#define ST7735_PWCTR6  0xFC

#define ST7735_GMCTRP1 0xE0
#define ST7735_GMCTRN1 0xE1

// Color definitions
#define	ST7735_BLACK   0x0000
#define	ST7735_BLUE    0x001F
#define	ST7735_RED     0xF800
#define	ST7735_GREEN   0x07E0
#define ST7735_CYAN    0x07FF
#define ST7735_MAGENTA 0xF81F
#define ST7735_YELLOW  0xFFE0
#define ST7735_WHITE   0xFFFF



//#define CS_HI easygpio_outputSet(15,1);
//#define CS_LO easygpio_outputSet(15,0);
//#define RST_HI
//#define RST_LO
#define DC_HI easygpio_outputSet(16,1);
#define DC_LO easygpio_outputSet(16,0);

 static uint8_t currentFont=0;
 static uint8_t x_moving=0;
 static uint8_t y_moving=0;
 static uint8_t x_master=0;

 static uint16_t RGBfore=0xffff;
 static uint16_t RGBback=0;
 static const char *ch32;

 static uint8_t mono=0;

 static uint8_t _height; // for the screen
 static uint8_t _width;

 static uint8_t cWidth; // for characters
 static uint8_t cHeight;

 static uint8_t _offset;
 static uint8_t bytes_high;
 static uint32_t c_size;

 static uint8_t colstart=0;
 static uint8_t rowstart=0;

#define swap(a,b) {a^=b; b^=a; a^=b;}

// int QDwidth=160;
// int QDheight=120;

/*
 * create if you don't have these elsewhere already
 *
 void IFA writeCommand(uint8_t c) {
 	DC_LO;
 	CS_LO;
   spi_tx8(1, c);
    CS_HI;
 }


 static void IFA writeData(uint8_t c) {
 	DC_HI;
 	CS_LO;
 	spi_tx8(1, c);
 	CS_HI;
 }
*/

 void IFA s6d02a1SetAddrWindow(uint8_t x0, uint8_t y0, uint8_t x1,uint8_t y1) {

	   writeCommand(QDTech_CASET); // Column addr set
	   spi_tx32(1,(x1+colstart)+((x0+colstart)<<16));
	   writeCommand(QDTech_RASET); // Row addr set
	   spi_tx32(1,(y1+colstart)+((y0+colstart)<<16));
	   writeCommand(QDTech_RAMWR); // write to RAM - do this here - may as well as can't READ from these devices

 }


 void IFA s6d02a1DrawHLine(uint8_t x0,uint8_t y0,uint16_t w){
	  s6d02a1SetAddrWindow(x0,y0,x0+w-1,y0);
	  for(int i=0; i<w; i++){
	    spi_tx16(1,RGBfore);

 	  }
  }

 void IFA s6d02a1DrawVLine(uint8_t x0,uint8_t y0,uint16_t h) {
	  s6d02a1SetAddrWindow(x0,y0,x0,y0+h-1);
 	  for(int i=0; i<h; i++){
	    spi_tx16(1,RGBfore);
 	  }
  }



 void IFA s6d02a1DrawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1) {

  	if (y0 == y1) {
  		if (x1 > x0) {
  		    s6d02a1DrawHLine(x0, y0, x1-x0+1);
  		} else if (x1 < x0) {
  		    s6d02a1DrawHLine(x1, y0, x0-x1+1);
  		} else {
  		    s6d02a1Dot(x0, y0);
  		}
  		return;
  	} else if (x0 == x1) {
  		if (y1 > y0) {
  			s6d02a1DrawVLine(x0, y0, y1-y0+1);
  		} else {
  			s6d02a1DrawVLine(x0, y1, y0-y1+1);
  		}
  		return;
  	}

  	uint8_t steep = abs(y1 - y0) > abs(x1 - x0);
  	if (steep) {
  		swap(x0, y0);
  		swap(x1, y1);
  	}
  	if (x0 > x1) {
  		swap(x0, x1);
  		swap(y0, y1);
  	}

  	int16_t dx, dy;
  	dx = x1 - x0;
  	dy = abs(y1 - y0);

  	int16_t err = dx / 2;
  	int16_t ystep;

  	if (y0 < y1) {
  		ystep = 1;
  	} else {
  		ystep = -1;
  	}

  	int16_t xbegin = x0;
  	if (steep) {
  		for (; x0<=x1; x0++) {
  			err -= dy;
  			if (err < 0) {
  				int16_t len = x0 - xbegin;
  				if (len) {
  				      s6d02a1DrawVLine(y0, xbegin, len + 1);
  				} else {
  					s6d02a1Dot(y0, x0);
  				}
  				xbegin = x0 + 1;
  				y0 += ystep;
  				err += dx;
  			}
  		}
  		if (x0 > xbegin + 1) s6d02a1DrawVLine(y0, xbegin, x0 - xbegin);
  	} else {
  		for (; x0<=x1; x0++) {
  			err -= dy;
  			if (err < 0) {
  				int16_t len = x0 - xbegin;
  				if (len) {
  				           s6d02a1DrawHLine(xbegin, y0, len + 1);
  				} else {
  				       s6d02a1Dot(x0, y0);
  				}
  				xbegin = x0 + 1;
  				y0 += ystep;
  				err += dx;
  			}
  		}
  		if (x0 > xbegin + 1) s6d02a1DrawHLine(xbegin, y0, x0 - xbegin);
  	}
  }


 void IFA s6d02a1FillScreen() { // fill screen in background colour
uint16_t tcol;
     tcol=RGBfore; RGBfore=RGBback;
	 s6d02a1FillRect(0, 0, _width, _height);
     RGBfore=tcol;
 }


 // draw a rectangle
  void IFA s6d02a1Rect(int16_t x, int16_t y, int16_t w, int16_t h) { // draw a rectangle in foreground colour
	 s6d02a1DrawVLine(x,y,h);
	 s6d02a1DrawVLine(x+w-1,y,h);
	 s6d02a1DrawHLine(x,y,w);
	 s6d02a1DrawHLine(x,y+h-1,w);
	 }

 // fill a rectangle
 void IFA s6d02a1FillRect(int16_t x, int16_t y, int16_t w, int16_t h) { // fill a rectangle in background colour
 uint16_t allchar;
   // rudimentary clipping (drawChar w/big text requires this)
   if((x >= _width) || (y >= _height)) return;
   if((x + w - 1) >= _width)  w = _width  - x;
   if((y + h - 1) >= _height) h = _height - y;
   s6d02a1Rect(x,y,w,h);
   s6d02a1SetAddrWindow(x+1, y+1, x+w-2, y+h-2);
   allchar=(h-2)*(w-2);
   while (allchar--) spi_tx16(1,RGBback);
 }


 void IFA s6d02a1Dot(uint16_t x, uint16_t y){ // draw a dot in foreground colour
	   s6d02a1SetAddrWindow(x,y,x,y);
	   spi_tx16(1,RGBfore);
 }


#define QD_DELAY 128

// nicked the idea of a table for startup parameters from elsewhere...  Saves lots of code.
static const uint8_t IRA s6d02a1InitVars[] = {
		    29,
			0xf0,	2,	0x5a, 0x5a,				// Excommand2
			0xfc,	2,	0x5a, 0x5a,				// Excommand3
			0x26,	1,	0x01,					// Gamma set
			0xfa,	15,	0x02, 0x1f,	0x00, 0x10,	0x22, 0x30, 0x38, 0x3A, 0x3A, 0x3A,	0x3A, 0x3A,	0x3d, 0x02, 0x01,	// Positive gamma control
			0xfb,	15,	0x21, 0x00,	0x02, 0x04,	0x07, 0x0a, 0x0b, 0x0c, 0x0c, 0x16,	0x1e, 0x30,	0x3f, 0x01, 0x02,	// Negative gamma control
			0xfd,	11,	0x00, 0x00, 0x00, 0x17, 0x10, 0x00, 0x01, 0x01, 0x00, 0x1f, 0x1f,							// Analog parameter control
			0xf4,	15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3f, 0x3f, 0x07, 0x00, 0x3C, 0x36, 0x00, 0x3C, 0x36, 0x00,	// Power control
			0xf5,	13, 0x00, 0x70, 0x66, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6d, 0x66, 0x06,				// VCOM control
			0xf6,	11, 0x02, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x02, 0x00, 0x06, 0x01, 0x00,							// Source control
			0xf2,	17, 0x00, 0x01, 0x03, 0x08, 0x08, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x04, 0x08, 0x08,	//Display control
			0xf8,	1,	0x11,					// Gate control
			0xf7,	4, 0xc8, 0x20, 0x00, 0x00,	// Interface control
			0xf3,	2, 0x00, 0x00,				// Power sequence control
			0x11,	QD_DELAY, 50,					// Wake
			0xf3,	2+QD_DELAY, 0x00, 0x01, 50,	// Power sequence control
			0xf3,	2+QD_DELAY, 0x00, 0x03, 50,	// Power sequence control
			0xf3,	2+QD_DELAY, 0x00, 0x07, 50,	// Power sequence control
			0xf3,	2+QD_DELAY, 0x00, 0x0f, 50,	// Power sequence control
			0xf4,	15+QD_DELAY, 0x00, 0x04, 0x00, 0x00, 0x00, 0x3f, 0x3f, 0x07, 0x00, 0x3C, 0x36, 0x00, 0x3C, 0x36, 0x00, 50,	// Power control
			0xf3,	2+QD_DELAY, 0x00, 0x1f, 50,	// Power sequence control
			0xf3,	2+QD_DELAY, 0x00, 0x7f, 50,	// Power sequence control
			0xf3,	2+QD_DELAY, 0x00, 0xff, 50,	// Power sequence control
			0xfd,	11, 0x00, 0x00, 0x00, 0x17, 0x10, 0x00, 0x00, 0x01, 0x00, 0x16, 0x16,							// Analog parameter control
			0xf4,	15, 0x00, 0x09, 0x00, 0x00, 0x00, 0x3f, 0x3f, 0x07, 0x00, 0x3C, 0x36, 0x00, 0x3C, 0x36, 0x00,	// Power control
			0x36,	1, 0x08,					// Memory access data control
			0x35,	1, 0x00,					// Tearing effect line on
			0x3a,	1+QD_DELAY, 0x05, 150,			// Interface pixel control
			0x29,	0,							// Display on
			0x2c,	0							// Memory write
	   };   // end

#define DELAY 0x80

static const uint8_t IRA Rcmd1[] = {                 // Init for 7735R, part 1 (red or green tab)
   15,                       // 15 commands in list:
   ST7735_SWRESET,   DELAY,  //  1: Software reset, 0 args, w/delay
     150,                    //     150 ms delay
   ST7735_SLPOUT ,   DELAY,  //  2: Out of sleep mode, 0 args, w/delay
     255,                    //     500 ms delay
   ST7735_FRMCTR1, 3      ,  //  3: Frame rate ctrl - normal mode, 3 args:
     0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
   ST7735_FRMCTR2, 3      ,  //  4: Frame rate control - idle mode, 3 args:
     0x01, 0x2C, 0x2D,       //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
   ST7735_FRMCTR3, 6      ,  //  5: Frame rate ctrl - partial mode, 6 args:
     0x01, 0x2C, 0x2D,       //     Dot inversion mode
     0x01, 0x2C, 0x2D,       //     Line inversion mode
   ST7735_INVCTR , 1      ,  //  6: Display inversion ctrl, 1 arg, no delay:
     0x07,                   //     No inversion
   ST7735_PWCTR1 , 3      ,  //  7: Power control, 3 args, no delay:
     0xA2,
     0x02,                   //     -4.6V
     0x84,                   //     AUTO mode
   ST7735_PWCTR2 , 1      ,  //  8: Power control, 1 arg, no delay:
     0xC5,                   //     VGH25 = 2.4C VGSEL = -10 VGH = 3 * AVDD
   ST7735_PWCTR3 , 2      ,  //  9: Power control, 2 args, no delay:
     0x0A,                   //     Opamp current small
     0x00,                   //     Boost frequency
   ST7735_PWCTR4 , 2      ,  // 10: Power control, 2 args, no delay:
     0x8A,                   //     BCLK/2, Opamp current small & Medium low
     0x2A,
   ST7735_PWCTR5 , 2      ,  // 11: Power control, 2 args, no delay:
     0x8A, 0xEE,
   ST7735_VMCTR1 , 1      ,  // 12: Power control, 1 arg, no delay:
     0x0E,
   ST7735_INVOFF , 0      ,  // 13: Don't invert display, no args, no delay
   ST7735_MADCTL , 1      ,  // 14: Memory access control (directions), 1 arg:
     0xC0,                   //     row addr/col addr, bottom to top refresh // REVERSED WAS 0c8
   ST7735_COLMOD , 1      ,  // 15: set color mode, 1 arg, no delay:
     0x05 };                //     16-bit color


static const uint8_t IRA Rcmd2green[] = {            // Init for 7735R, part 2 (green tab only)
   2,                        //  2 commands in list:
   ST7735_CASET  , 4      ,  //  1: Column addr set, 4 args, no delay:
     0x00, 0x02,             //     XSTART = 0
     0x00, 0x7F+0x02,        //     XEND = 127
   ST7735_RASET  , 4      ,  //  2: Row addr set, 4 args, no delay:
     0x00, 0x01,             //     XSTART = 0
     0x00, 0x9F+0x01 };     //     XEND = 159

static const uint8_t IRA Rcmd3[] = {                 // Init for 7735R, part 3 (red or green tab)
    4,                        //  4 commands in list:
    ST7735_GMCTRP1, 16      , //  1: Magical unicorn dust, 16 args, no delay:
      0x02, 0x1c, 0x07, 0x12,
      0x37, 0x32, 0x29, 0x2d,
      0x29, 0x25, 0x2B, 0x39,
      0x00, 0x01, 0x03, 0x10,
    ST7735_GMCTRN1, 16      , //  2: Sparkles and rainbows, 16 args, no delay:
      0x03, 0x1d, 0x07, 0x06,
      0x2E, 0x2C, 0x29, 0x2D,
      0x2E, 0x2E, 0x37, 0x3F,
      0x00, 0x00, 0x02, 0x10,
    ST7735_NORON  ,    DELAY, //  3: Normal display on, no args, w/delay
      10,                     //     10 ms delay
    ST7735_DISPON ,    DELAY, //  4: Main screen turn on, no args w/delay
      100 };


static const uint8_t IRA st7735bInitVars[] = {
  18,                       // 18 commands in list:
  ST7735_SWRESET,   DELAY,  //  1: Software reset, no args, w/delay
    50,                     //     50 ms delay
  ST7735_SLPOUT ,   DELAY,  //  2: Out of sleep mode, no args, w/delay
    255,                    //     255 = 500 ms delay
  ST7735_COLMOD , 1+DELAY,  //  3: Set color mode, 1 arg + delay:
    0x05,                   //     16-bit color
    10,                     //     10 ms delay
  ST7735_FRMCTR1, 3+DELAY,  //  4: Frame rate control, 3 args + delay:
    0x00,                   //     fastest refresh
    0x06,                   //     6 lines front porch
    0x03,                   //     3 lines back porch
    10,                     //     10 ms delay
  ST7735_MADCTL , 1      ,  //  5: Memory access ctrl (directions), 1 arg:
    0x08,                   //     Row addr/col addr, bottom to top refresh
  ST7735_DISSET5, 2      ,  //  6: Display settings #5, 2 args, no delay:
    0x15,                   //     1 clk cycle nonoverlap, 2 cycle gate
                            //     rise, 3 cycle osc equalize
    0x02,                   //     Fix on VTL
  ST7735_INVCTR , 1      ,  //  7: Display inversion control, 1 arg:
    0x0,                    //     Line inversion
  ST7735_PWCTR1 , 2+DELAY,  //  8: Power control, 2 args + delay:
    0x02,                   //     GVDD = 4.7V
    0x70,                   //     1.0uA
    10,                     //     10 ms delay
  ST7735_PWCTR2 , 1      ,  //  9: Power control, 1 arg, no delay:
    0x05,                   //     VGH = 14.7V, VGL = -7.35V
  ST7735_PWCTR3 , 2      ,  // 10: Power control, 2 args, no delay:
    0x01,                   //     Opamp current small
    0x02,                   //     Boost frequency
  ST7735_VMCTR1 , 2+DELAY,  // 11: Power control, 2 args + delay:
    0x3C,                   //     VCOMH = 4V
    0x38,                   //     VCOML = -1.1V
    10,                     //     10 ms delay
  ST7735_PWCTR6 , 2      ,  // 12: Power control, 2 args, no delay:
    0x11, 0x15,
  ST7735_GMCTRP1,16      ,  // 13: Magical unicorn dust, 16 args, no delay:
    0x09, 0x16, 0x09, 0x20, //     (seriously though, not sure what
    0x21, 0x1B, 0x13, 0x19, //      these config values represent)
    0x17, 0x15, 0x1E, 0x2B,
    0x04, 0x05, 0x02, 0x0E,
  ST7735_GMCTRN1,16+DELAY,  // 14: Sparkles and rainbows, 16 args + delay:
    0x0B, 0x14, 0x08, 0x1E, //     (ditto)
    0x22, 0x1D, 0x18, 0x1E,
    0x1B, 0x1A, 0x24, 0x2B,
    0x06, 0x06, 0x02, 0x0F,
    10,                     //     10 ms delay
  ST7735_CASET  , 4      ,  // 15: Column addr set, 4 args, no delay:
    0x00, 0x02,             //     XSTART = 2
    0x00, 0x81,             //     XEND = 129
  ST7735_RASET  , 4      ,  // 16: Row addr set, 4 args, no delay:
    0x00, 0x02,             //     XSTART = 1
    0x00, 0x81,             //     XEND = 160
  ST7735_NORON  ,   DELAY,  // 17: Normal display on, no args, w/delay
    10,                     //     10 ms delay
  ST7735_DISPON ,   DELAY,  // 18: Main screen turn on, no args, w/delay
    255 };



static const uint8_t IRA ILI9163[]=
	{
	17,             // 17 commands follow
	0x01,  0,       // Software reset
	0x11,  0,       // Exit sleep mode
	0x3A,  1, 0x05, // Set pixel format
	0x26,  1, 0x04, // Set Gamma curve
	0xF2,  1, 0x01, // Gamma adjustment enabled
	0xE0, 15, 0x3F, 0x25, 0x1C, 0x1E, 0x20, 0x12, 0x2A, 0x90,
	          0x24, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, // Positive Gamma
	0xE1, 15, 0x20, 0x20, 0x20, 0x20, 0x05, 0x00, 0x15,0xA7,
	          0x3D, 0x18, 0x25, 0x2A, 0x2B, 0x2B, 0x3A, // Negative Gamma
	0xB1,  2, 0x08, 0x08, // Frame rate control 1
	0xB4,  1, 0x07,       // Display inversion
	0xC0,  2, 0x0A, 0x02, // Power control 1
	0xC1,  1, 0x02,       // Power control 2
	0xC5,  2, 0x50, 0x5B, // Vcom control 1
	0xC7,  1, 0x40,       // Vcom offset
	0x2A,  4+DELAY, 0x00, 0x00, 0x00, 0x7F, 250, // Set column address
	0x2B,  4, 0x00, 0x00, 0x00, 0x9F,            // Set page address
	0x36,  1, 0xC8,       // Set address mode
	0x29,  0,             // Set display on
	};


void IFA s6d02a1Init(const char *ssdp) {

	   uint8_t nxt;
	   uint8_t numc;
	   uint8_t numa;
	   uint16_t ms;

	 // ssdp=s6d02a1InitVars;

	   numc=read_rom_uint8(ssdp++);
	   while (numc--)
   	   {
	   writeCommand(read_rom_uint8(ssdp++));
	   numa=read_rom_uint8(ssdp++);
	   ms=numa&128;
	   numa&=127;
	   while (numa--) writeData(read_rom_uint8(ssdp++));
	   if (ms) { ms=read_rom_uint8(ssdp++); if (ms==255) ms=500;  os_delay_us(1000*ms); }
   	   }
 }


 // pack the colours rgb into 5-6-5 bits in a 16 bit var
 void IFA s6d02a1Fore(uint16_t r, uint16_t g, uint16_t b)
 {
	 RGBfore=((r&0xf8)<<8)|((g&0xfc)<<3)|(b>>3);
 }
 void IFA s6d02a1Back(uint16_t r, uint16_t g, uint16_t b)
 {
	 RGBback=((r&0xf8)<<8)|((g&0xfc)<<3)|(b>>3);
 }

 void IFA s6d02a1Cursor(uint8_t x, uint8_t y)
 {
	x_master=x;  x_moving=x; y_moving=y;
 }



 uint8_t IFA s6d02a1GCLDChar(uint8_t ch){
        uint8_t cwid;
 	    uint8_t i;
 	    uint8_t var_width;
        uint8_t t_height;
 		uint16_t x;
 		uint16_t y;
 		const char *lch32;

 		lch32=ch32;
        x=x_moving; y=y_moving;
 	    lch32+=((ch-_offset)*c_size);
 	    cwid=read_rom_uint8(lch32++);
 	    if (mono) var_width=cWidth; else var_width=cwid; // this should speed up non-mono output - less columns

        // high speed char drawing normal window for vertical first - but I can't seem to find the register bit to do vertical
 	    // without messing everything up - so - making 1px wide windows further down - slower but not by much....
 	   //s6d02a1SetAddrWindow(x, y, x+var_width-1, y+cHeight-1);

   	    for ( i = 0; i < var_width; i++ ) {
                       uint32_t j;
                       uint16_t xi=x+i;
                       const char *ib=lch32+(i*bytes_high);
                       t_height=cHeight; // only do ACTUALLY required height
                       s6d02a1SetAddrWindow(x+i, y, x+i, y+t_height-1);
                       for ( j = 0; j < bytes_high; j++ ) {
                       	   	   uint16_t jy=(j<<3)+y;
                               uint8_t dat = read_rom_uint8( ib + j );
                               uint8_t bit;
                               for (bit = 0; bit < 8; bit++) {
                                      if (t_height)
										 {
                                          spi_tx16(1, (dat & (1<<bit)) ? RGBfore : RGBback );
										  --t_height;
										 }
                               }
                       }
               }
  x_moving+=var_width;  return cwid;
 }


 void IFA s6d02a1GCLDFont(uint8_t font)
 {
	currentFont=font;  // save for switching elsewhere
	switch (font)
	{
	case 1 : ch32=digital36x69; break;
	case 2 : ch32=digital17x32; break;
	case 3 : ch32=trebuchet; break;
	case 4 : ch32=calibri21x22; break;
	case 5 : ch32=calibri32x34; break;
	case 6 : ch32=petesfont24x28; break;
	case 7 : ch32=meteocons21x25; break;
	case 8 : ch32=ms_reference_sans_serif16x16; break;
	case 9 : ch32=Basic8x8; break;
	case 10: ch32=assorted1_19x21; break;
	case 11: ch32=assorted2_19x21; break;
	case 12: ch32=assorted3_19x21; break;
	case 0 :
	default: ch32=calibri16x17; break;
	}
    cWidth=read_rom_uint8(ch32++);
    cHeight=read_rom_uint8(ch32++);
    _offset=read_rom_uint8(ch32++);
	bytes_high=(cHeight+7)>>3; // rounded up bytes for a vertical slice of the char
    c_size=((cWidth*bytes_high)+1);
 }

 void IFA s6d02a1Glyph(uint8_t wf, uint8_t x)
 {
	 uint8_t tfont;
	 tfont=currentFont;
	 switch (wf)
	 {
	 case 1 :s6d02a1GCLDFont(10); break;
	 case 2 :s6d02a1GCLDFont(11); break;
	 case 3 :s6d02a1GCLDFont(12); break;
	 default :s6d02a1GCLDFont(10); break;
	 }
	 s6d02a1GCLDChar(x);
	 s6d02a1GCLDFont(tfont);
 }

 static uint32_t thevalue;

 static IFA char *parse( char *p)
 		{
 	    thevalue=0;
 	    while ((*p>='0')&&(*p<='9')) { thevalue*=10; thevalue+=(*p)-'0'; p++; }
 		if (*p==';') return ++p;
 		if (*p==',') return ++p;
 		return p;
 		}


 void IFA s6d02a1GCLDString(uint8_t* st){
	 char tstore[12];
	 while (*st)
 	 {
	 if (*st=='\n')  { x_moving=x_master; y_moving+=cHeight; st++; continue;  }
	 if (*st=='\r')  { st++; continue;  }
	 switch (*st)
	 	 {
	 	 case '$' : switch (*++st)
						 {
	 	 	 	 	 	 case '$' : break;
	 	 	 	 	 	 //case 'l' : s6d02a1FillScreen(); ++st;
			             //           continue;

	 	 	 	 	 	 case 'S' : s6d02a1Startup(s6d02a1InitVars);
	 	 			                os_delay_us(100000);
	 	 			                s6d02a1Back(0,0,0);
	 	 			                s6d02a1FillScreen();
	 	 			                s6d02a1Cursor(0,0);
	 	 			                ++st;
	 	 			                continue;

	 	 	 	 	 	 case 's' : s6d02a1Startup(Rcmd1);  //doesn't work on my board
	 	 	 	 	 	 	 	 	s6d02a1Init(Rcmd2green);
	 	 	 	 	 	 	 	 	s6d02a1Init(Rcmd3);
	 	 			                os_delay_us(100000);
	 	 			                s6d02a1Back(0,0,0);
	 	 			                s6d02a1FillScreen();
	 	 			                s6d02a1Cursor(0,0);
	 	 			                ++st;
	 	 			                continue;

	 	 	 	 	 	 case 'i' : s6d02a1Startup(ILI9163); // doesn't work on my board
	 	 			                os_delay_us(100000);
	 	 			                s6d02a1Back(0,0,0);
	 	 			                s6d02a1FillScreen();
	 	 			                s6d02a1Cursor(0,0);
	 	 			                ++st;
	 	 			                continue;

	 	 	 	 	 	 case 'P' : st=parse(++st); int xx=thevalue; st=parse(st); int yy=thevalue; x_master=xx; x_moving=xx; y_moving=yy; continue;
	 	 	 	 	 	 case 'C' : st=parse(++st); int rr=thevalue; st=parse(st); int gg=thevalue; st=parse(st); int bb=thevalue; s6d02a1Fore(rr,gg,bb); continue;
	 	 	 	 	 	 case 'B' : st=parse(++st); rr=thevalue; st=parse(st); gg=thevalue; st=parse(st); bb=thevalue; s6d02a1Back(rr,gg,bb); continue;
	 	 	 	 	 	 case 'M' : st=parse(++st); if (thevalue==1) mono=1; else mono=0; continue;
	 	 	 	 	     case 'F' : st=parse(++st); if (thevalue<=12) s6d02a1GCLDFont(thevalue); continue;
	 	 	 	 	     case 'R' : st=parse(++st); if (thevalue!=1) thevalue=0;  s6d02a1SetRotation(thevalue); continue;
	 	 	 	 	     case 'L' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); int xx2=thevalue; st=parse(st); int yy2=thevalue;
	 	 	 	 	     	 	 	s6d02a1DrawLine(xx,yy,xx2,yy2);continue;
	 	 	 	 	     case 'E' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
	 	 	 	 	     	 	 	s6d02a1Rect(xx,yy,xx2,yy2);continue;	 // 2nd param width and height
	 	 	 	 	     case 'I' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
	 	 	 	 	                s6d02a1FillRect(xx,yy,xx2,yy2); continue;	 // 2nd param width and height
	 	 	 	 	     case 'W' : s6d02a1FillRect(0,0,_width, _height); ++st; continue;

	 	 	 	 	 	 case 'T' : ++st; os_sprintf(tstore,"%02d:%02d:%02d",tm.Hour, tm.Minute, tm.Second); s6d02a1GCLDString(tstore); continue;
	 	 	 	 	 	 case 'D' : ++st; os_sprintf(tstore,"%02d/%02d/%02d",tm.Day, tm.Month, tm.Year); s6d02a1GCLDString(tstore); continue;
	 	 	 	 	 	 case 'A' : ++st; struct ip_info theIp; wifi_get_ip_info(0, &theIp);
	 	 	 	 	 				os_sprintf(tstore, "%d:%d:%d:%d", theIp.ip.addr & 255, (theIp.ip.addr >> 8) & 255, (theIp.ip.addr >> 16) & 255, theIp.ip.addr >> 24);
	 	 	 	 	 			    s6d02a1GCLDString(tstore); continue;
	 	 	 	 	     case 'G' : st=parse(++st); xx=thevalue; if ((xx<1) || (xx>3)) continue;
	 	 	 	 	 	 	 	 	if (*st==0) continue;
	 	 	 	 	 	 	 	    s6d02a1Glyph(xx,*st++); continue;
	 	 	 	 	 	 case 'O' : st=parse(++st); rr=thevalue; st=parse(st); gg=thevalue; s6da1scope(rr,gg); continue;
	 	 	 	 	 	 default: break;

						 }
	 	 default:  break;
	 	 }
	 s6d02a1GCLDChar(*st++);
	 }
 }



void IFA s6d02a1Startup(const uint8_t *ssdp)
{
	  sysCfg.enable13 = 1; // stop 13 use as an indicator
	  spiActive = 1;
	  easygpio_pinMode(16, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	  easygpio_pinMode(0,EASYGPIO_NOPULL, EASYGPIO_OUTPUT);

		easygpio_outputSet(0,0);
		os_delay_us(100000);
		easygpio_outputSet(0,1);
		os_delay_us(50000);
		easygpio_pinMode(0,EASYGPIO_PULLUP,EASYGPIO_OUTPUT);

	  spi_init(1); // HSPI mode
	  spi_clock(1,1,2); // will not work without this - too fast for display
	  s6d02a1Init(ssdp);
	  s6d02a1GCLDFont(0); // select default string
	  s6d02a1SetRotation(1);
	  spiActive=0;
	  for (int a=0;a<10;a++) qd_scopelines[a]=255; // experiment
}



void IFA s6d02a1SetRotation(uint8_t m) {
// Generally 0 - Portrait 1 - Landscape

  writeCommand(QDTech_MADCTL);
  uint8_t rotation = m % 4; // can't be higher than 3
  switch (rotation) {
   case 0:
     writeData(MADCTL_MX | MADCTL_MY | MADCTL_BGR);
     _width  = QDTech_TFTWIDTH;
     _height = QDTech_TFTHEIGHT;
     break;
   case 1:
   writeData(MADCTL_MY | MADCTL_MV | MADCTL_BGR);
   _width  = QDTech_TFTHEIGHT;
     _height = QDTech_TFTWIDTH;
     break;
  case 2:
     writeData(MADCTL_BGR);
     _width  = QDTech_TFTWIDTH;
     _height = QDTech_TFTHEIGHT;
    break;
   case 3:
//     writedata(MADCTL_MX | MADCTL_MV | MADCTL_RGB);
     writeData(MADCTL_MX | MADCTL_MV | MADCTL_BGR);
     _width  = QDTech_TFTHEIGHT;
     _height = QDTech_TFTWIDTH;
     break;
  }
}


// not actually using this as default is scroll window is full screen - but you never know...
int IFA s6d02a1SetScrollWindow(uint8_t TopFixHeight, uint8_t BotFixHeight)
{
// Calculate VScroll height.
int VScrollHeight = QDTech_TFTHEIGHT - TopFixHeight - BotFixHeight;
	// scroll mode
	writeCommand(QDTech_VSCRDEF);
	// Top Fixed Area
	writeData(0x00);
	spi_tx8(1,TopFixHeight);
	// Vertical Scroll Area
	writeData(0x00);
	spi_tx8(1,VScrollHeight);
	// Bottom Fixed Area
	writeData(0x00);
	spi_tx8(1,BotFixHeight);
	return VScrollHeight;
}

void IFA s6d02a1VScroll(uint8_t vsp) {
// VSCRSADD (Vertical Scrolling Start Address)
	writeCommand(QDTech_VSCRSADD);
	spi_tx16(1,vsp);
}


void IFA s6d02a1ExitScrollMode()  {
	writeCommand(QDTech_DISPOFF);
	writeCommand(QDTech_NORON);		//normal mode on
	writeCommand(QDTech_DISPON);
}

void IFA s6da1scope(uint8_t line, uint8_t val)
{
uint16_t tRGBfore;
tRGBfore=RGBfore;
switch (line)
	{
    case 0 : qd_scopepos++; qd_scopepos%=160;
			 s6d02a1Fore(0,0,0); s6d02a1DrawVLine(qd_scopepos,0,160);
			 s6d02a1VScroll((qd_scopepos+1)%160);
			 break;
    case 1 : s6d02a1Fore(255,0,0); break;
	case 2 : s6d02a1Fore(0,255,0); break;
	case 3 : s6d02a1Fore(0,0,255); break;
	case 4 : s6d02a1Fore(255,255,0); break;
	case 5 : s6d02a1Fore(255,0,255); break;
	case 6 : s6d02a1Fore(0,255,255); break;
	case 7 : s6d02a1Fore(255,40,100); break;
	case 8 : s6d02a1Fore(100,255,40); break;
	case 9 : s6d02a1Fore(255,255,255); break;
	case 10: s6d02a1ExitScrollMode(); for (int a=0;a<10;a++) qd_scopelines[a]=255;
	         s6d02a1FillScreen(); s6d02a1Cursor(0,0); return; // reset back to normal
	}

int y1;
int y2;
if (qd_scopelines[line]==255) qd_scopelines[line]=val;
y1=qd_scopelines[line]; y2=val; qd_scopelines[line]=val;
if (y1>y2) swap(y1,y2);
y2=y2-y1; if (y2==0) y2++;
s6d02a1DrawVLine(qd_scopepos,y1,y2);
RGBfore=tRGBfore;
}

